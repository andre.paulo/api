﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class Sensor
    {
        [Key]
        public int Id_Sensor { get; set; }
        [Timestamp]
        public byte[] Tempo { get; set; }

        //public TimestampAttribute Tempo { get; set; }
        public string Tag { get; set; }
        public string Valor { get; set; }
    }
}
