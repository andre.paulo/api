﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Models;

namespace API.Repositorio
{
    public class SensorRepositorio : ISensorRepository
    {
        private readonly SensorDbContext _contexto;
        public SensorRepositorio(SensorDbContext ctx)
        {
            _contexto = ctx;
        }
        public void Add(Sensor sensor)
        {
            _contexto.Sensor.Add(sensor);
            _contexto.SaveChanges();
        }

        public Sensor Find(long id)
        {
            return _contexto.Sensor.FirstOrDefault(s => s.Id_Sensor == id);
        }

        public IEnumerable<Sensor> GetAll()
        {
            return _contexto.Sensor.ToList();
        }

        public void Remove(long id)
        {
            var entity = _contexto.Sensor.First(s => s.Id_Sensor == id);
            _contexto.Sensor.Remove(entity);
            _contexto.SaveChanges();
        }

        public void Update(Sensor sensor)
        {
            _contexto.Sensor.Update(sensor);
            _contexto.SaveChanges();
        }
    }
}
