﻿using API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Repositorio
{
    public interface ISensorRepository
    {
        void Add(Sensor sensor);

        IEnumerable<Sensor> GetAll();

        Sensor Find(long id);

        void Remove(long id);

        void Update(Sensor sensor);
        
    }
}
