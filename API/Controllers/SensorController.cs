﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Models;
using API.Repositorio;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //public class SensorController : ControllerBase
    public class SensorController : Controller

    {
        private readonly ISensorRepository _sensorRepositorio;

        public SensorController(ISensorRepository sensorRepo)
        {
            _sensorRepositorio = sensorRepo;

        }

        [HttpGet]

        public IEnumerable<Sensor> GetAll()
        {
            return _sensorRepositorio.GetAll();
        }

        [HttpGet("{id}", Name ="GetSensor")]

        public IActionResult GetById(long id)
        {
            var sensor = _sensorRepositorio.Find(id);
            if (sensor==null)
            
                return NotFound();
                return new ObjectResult(sensor);           
        }

        [HttpPost]
        public IActionResult Create([FromBody] Sensor sensor)
        {
            if (sensor == null)
            
                return BadRequest();

            _sensorRepositorio.Add(sensor);

            return CreatedAtRoute("GetSensor", new {id=sensor.Id_Sensor}, sensor);
            
        }

        [HttpPut]

        public IActionResult Update (long id, [FromBody] Sensor sensor)
        {
            if (sensor == null || sensor.Id_Sensor != id)
                return BadRequest();

                var _sensor = _sensorRepositorio.Find(id);

            if (sensor == null)
                return NotFound();

            _sensor.Tag = sensor.Tag;
            _sensor.Valor = sensor.Valor;

            _sensorRepositorio.Update(_sensor);

            return new NoContentResult();

        }

        [HttpDelete("{id}")]

        public IActionResult Delete (long id)
        {
            var sensor = _sensorRepositorio.Find(id);

            if (sensor == null)
                return NotFound();

            _sensorRepositorio.Remove(id);
            return new NoContentResult();

        }

    }
}