﻿CREATE TABLE [dbo].[Sensor]
(
	[SensorId] INT NOT NULL PRIMARY KEY, 
    [TimeStamp] DATETIME NULL, 
    [Tag] NVARCHAR(MAX) NULL, 
    [Valor] NVARCHAR(MAX) NULL
)

--insert  into Sensor ('2019-01-01', andre.lepesteur, rj); 

--select * from Sensor